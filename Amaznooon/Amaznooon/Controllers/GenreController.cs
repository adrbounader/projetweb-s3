﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    public class GenreController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Genre/

        public ActionResult Index()
        {
            DbSqlQuery<Genre> genre = db.Genre.SqlQuery("SELECT DISTINCT Genre.* FROM Genre JOIN Musicien ON Genre.Code_Genre = Musicien.Code_Genre ORDER BY Genre.Libellé_Abrégé ;");
            return View(genre.ToList());
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}