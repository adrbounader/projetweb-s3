﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using Amaznooon.Models;


namespace Amaznooon.Controllers
{
    [Authorize]
    public class CompteController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Compte/

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && Login(model.UserName, model.Password))
            {
                createSession(getUser(model.UserName));
                return RedirectToLocal(returnUrl);
            }

            // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
            ModelState.AddModelError("", "Le nom d'utilisateur ou mot de passe fourni est incorrect.");
            return View(model);
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            ViewBag.Pays = getPays();

            if (ModelState.IsValid)
            {
                Abonné user = new Abonné();

                if (model.Login != null)
                    user.Login = model.Login.ToString();

                if (model.Password != null)
                    user.Password = model.Password.ToString();

                user.Nom_Abonné = model.Nom.ToString();

                if (model.Prénom != null)
                    user.Prénom_Abonné = model.Prénom.ToString();

                if (model.Email != null)
                    user.Email = model.Email.ToString();

                if (model.Pays != null)
                    user.Code_Pays = model.Pays;

                if (model.Ville != null)
                    user.Ville = model.Ville.ToString();

                if (model.CP != null)
                    user.Code_Postal = model.CP.ToString();

                db.Abonné.Add(user);

                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Inscription échouée");

                    // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
                    return View(model);
                }
                createSession(user);

                return RedirectToAction("Index", "Home");
            }
            // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
            return View(model);
        }

        //
        // GET: /Account/Register

        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.Pays = getPays();

            return View();
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logoff()
        {
            Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(RegisterModel model)
        {
            Abonné user = getUser(model.Login);

            // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
            return View(user);
        }

        public ActionResult UserInformation()
        {
            Abonné user = this.Session["User"] as Abonné;

            if (user != null)
                return View(user);
            else
                return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult UpdateUserInformation()
        {
            ViewBag.Pays = getPays();

            Abonné user = this.Session["User"] as Abonné;
            ViewBag.Abonné = user;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserInformation(UpdateModel model)
        {
            Abonné user = this.Session["User"] as Abonné;
            ViewBag.Abonné = user;

            ViewBag.Pays = getPays();

            if (ModelState.IsValid)
            {
                Abonné User = db.Abonné.Find(user.Code_Abonné);

                if (model.NewPassword == null)
                    user.Password = model.OldPassword;
                else
                    user.Password = model.NewPassword;


                    if (model.Login != null)
                        User.Login = model.Login.ToString();

                    if (model.NewPassword != null)
                        User.Password = model.NewPassword.ToString();

                    if (model.Nom != null)
                        User.Nom_Abonné = model.Nom.ToString();

                    if (model.Prénom != null)
                        User.Prénom_Abonné = model.Prénom.ToString();

                    if (model.Email != null)
                        User.Email = model.Email.ToString();

                    if (model.Pays != null)
                        User.Code_Pays = model.Pays;

                    if (model.Ville != null)
                        User.Ville = model.Ville.ToString();

                    if (model.CP != null)
                        User.Code_Postal = model.CP.ToString();


                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Inscription échouée");

                    // Si nous sommes arrivés là, quelque chose a échoué, réafficher le formulaire
                    return View(model);
                }

                    this.Session["User"] = User;
                    this.Session["UserName"] = User.Nom_Abonné;

                    return RedirectToAction("UserInformation"); 

            }
            return View(model);          
        }


        #region Applications auxiliaires
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private Abonné getUser(string login)
        {
            Abonné user = null;
            try
            {
                user = db.Abonné.Where(a => a.Login == login).Single();
            }
            catch (System.InvalidOperationException)
            {
                return null;
            }
            return user;
        }

        private bool Login(string login, string password)
        {
            Abonné user = getUser(login);
            string pass = "";

            if (user != null)
                pass = db.Abonné.Where(a => a.Login == login).Single().Password;

            return password.Equals(pass.Replace(" ", ""));
        }

        private void createSession(Abonné User)
        {
            Abonné user = User;

            List<Enregistrement> panier = new List<Enregistrement>();

            FormsAuthentication.SetAuthCookie(user.Nom_Abonné, true);

            this.Session["User"] = user;
            this.Session["UserName"] = user.Nom_Abonné;
            this.Session["Panier"] = panier;
            this.Session["NombreArticles"] = panier.Count();
        }

        private bool HasLocalAccount()
        {
            return Session["UserProfile"] != null;
        }

        private void Logout()
        {
            FormsAuthentication.SignOut();

            //Clears out Session
            this.Session.Clear();
            this.Session.Abandon();
        }

        private List<SelectListItem> getPays()
        {
            List<SelectListItem> pays = new List<SelectListItem>();

            foreach (Pays Pays in db.Pays)
            {
                SelectListItem selectList = new SelectListItem()
                {
                    Text = Pays.Nom_Pays,
                    Value = Pays.Code_Pays.ToString()
                };
                pays.Add(selectList);
            }

            return pays;
        }
        #endregion
    }
}
