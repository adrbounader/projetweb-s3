﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    public class OrchestreController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Orchestre/

        public ActionResult Index(String initiale = null)
        {
            DbSqlQuery<Orchestres> orchestre = null;
            if (initiale != null)
                orchestre = db.Orchestres.SqlQuery("SELECT DISTINCT * FROM Orchestres WHERE Orchestres.Nom_Orchestre LIKE '" + initiale + "%' ORDER BY Orchestres.Nom_Orchestre ;");
            else
                orchestre = db.Orchestres.SqlQuery("SELECT DISTINCT * FROM Orchestres ORDER BY Orchestres.Nom_Orchestre ;");

            return View(orchestre.ToList());
        }

        //
        // GET: /Orchestre/Details/5

        public ActionResult Details(int id = 0)
        {
            Orchestres orchestres = db.Orchestres.Find(id);
            if (orchestres == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Direction ON Enregistrement.Code_Morceau = Direction.Code_Morceau JOIN Orchestres ON Direction.Code_Orchestre = Orchestres.Code_Orchestre WHERE Orchestres.Code_Orchestre = " + id + " ;");
            ViewBag.Extrait = albums.ToList();

            return View(orchestres);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
