﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    public class MusicienController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        public class BinaryResult : ActionResult
        {
            public string ContentType { get; set; }
            public byte[] Data { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = ContentType;

                if (Data != null)
                    response.BinaryWrite(Data);
            }
        }

        public ActionResult Photo(int id)
        {
            // On récupère la photo dans la base de donnée et on la retourne telle quelle.
            byte[] photo = db.Musicien.Where(m => m.Code_Musicien == id).Single().Photo;

            return new BinaryResult() { ContentType = "image/jpeg", Data = photo };
        }

        public ActionResult Interprete(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
            {
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Interpréter ON Musicien.Code_Musicien = Interpréter.Code_Musicien WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Nom_Musicien ;");
            }
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Interpréter ON Musicien.Code_Musicien = Interpréter.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }

        public ActionResult Compositeur(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Composer ON Musicien.Code_Musicien = Composer.Code_Musicien WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Nom_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Composer ON Musicien.Code_Musicien = Composer.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }

        public ActionResult ChefOrchestre(String initiale = null)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (initiale != null)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Direction ON Musicien.Code_Musicien = Direction.Code_Musicien WHERE Musicien.Nom_Musicien LIKE '" + initiale + "%' ORDER BY Musicien.Nom_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien JOIN Direction ON Musicien.Code_Musicien = Direction.Code_Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }

        public ActionResult Musicien(int id = -1)
        {
            DbSqlQuery<Musicien> musicien = null;
            if (id != -1)
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien WHERE Musicien.Code_Instrument = '" + id + "' ORDER BY Musicien.Nom_Musicien ;");
            else
                musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* FROM Musicien ORDER BY Musicien.Nom_Musicien ;");

            return View(musicien.ToList());
        }

        //
        // GET: /Musicien/Details/5

        public ActionResult Details(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Interpréter ON Enregistrement.Code_Morceau = Interpréter.Code_Morceau JOIN Musicien ON Interpréter.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + " ;");
            ViewBag.Extrait = albums.ToList();

            return View(musicien);
        }

        public ActionResult DetailsCompositeurs(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Composition ON Enregistrement.Code_Composition = Composition.Code_Composition JOIN Composition_Oeuvre ON Composition.Code_Composition = Composition_Oeuvre.Code_Composition JOIN Oeuvre ON Composition_Oeuvre.Code_Oeuvre = Oeuvre.Code_Oeuvre JOIN Composer ON Oeuvre.Code_Oeuvre = Composer.Code_Oeuvre JOIN Musicien ON Composer.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + ";");
            ViewBag.Extrait = albums.ToList();

            return View(musicien);
        }

        public ActionResult DetailsChef(int id = 0)
        {
            Musicien musicien = db.Musicien.Find(id);
            if (musicien == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Album> albums = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Direction ON Enregistrement.Code_Morceau = Direction.Code_Morceau JOIN Musicien ON Direction.Code_Musicien = Musicien.Code_Musicien WHERE Musicien.Code_Musicien = " + id + ";");
            ViewBag.Extrait = albums.ToList();

            return View(musicien);
        }

        public ActionResult Epoque()
        {
            return View();
        }

        public ActionResult CompositeurParEpoque(int epoque)
        {
            int[,] tabEpoque = new int[,] { { 0, 399 }, { 400, 1499 }, { 1500, 1699 }, { 1700, 1799 }, { 1800, 1899 }, { 1900, 1999 } };

            DbSqlQuery<Musicien> musiciens = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* " +
                                                                 "FROM Musicien " +
                                                                 "JOIN Composer ON Musicien.Code_Musicien = Composer.Code_Musicien " +
                                                                 "WHERE Musicien.Année_Naissance<>Musicien.Année_Mort " +
                                                                            "AND Musicien.Année_Naissance BETWEEN '" + tabEpoque[epoque, 0] + "' AND '" + tabEpoque[epoque, 1] + "' " +
                                                                 "ORDER BY Musicien.Nom_Musicien;");

            return View(musiciens.ToList());
        }

        public ActionResult InterpreteParEpoque(int epoque)
        {
            int[,] tabEpoque = new int[,] { { 0, 1899 }, { 1900, 1909 }, { 1910, 1919 }, { 1920, 1929 }, { 1930, 1939 }, { 1940, 1949 }, { 1950, 1959 },
                                            { 1960,1969 }, { 1970, 1979 }, { 1980, 1989 }, { 1990, 2014 } };

            DbSqlQuery<Musicien> musicien = db.Musicien.SqlQuery("SELECT DISTINCT Musicien.* " +
                                                                 "FROM Musicien " +
                                                                 "JOIN Interpréter ON Musicien.Code_Musicien = Interpréter.Code_Musicien " +
                                                                 "WHERE Musicien.Année_Naissance<>Musicien.Année_Mort " +
                                                                            "AND Musicien.Année_Naissance BETWEEN '" + tabEpoque[epoque, 0] + "' AND '" + tabEpoque[epoque, 1] + "' " +
                                                                 "ORDER BY Musicien.Nom_Musicien;");

            return View(musicien.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}