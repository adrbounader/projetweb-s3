//------------------------------------------------------------------------------
// <auto-generated>
//    Ce code a été généré à partir d'un modèle.
//
//    Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//    Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Amaznooon
{
    using System;
    using System.Collections.Generic;
    
    public partial class Genre
    {
        public Genre()
        {
            this.Album = new HashSet<Album>();
            this.Musicien = new HashSet<Musicien>();
        }
    
        public int Code_Genre { get; set; }
        public string Libellé_Abrégé { get; set; }
    
        public virtual ICollection<Album> Album { get; set; }
        public virtual ICollection<Musicien> Musicien { get; set; }
    }
}
