﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    public class AlbumController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Album/

        public ActionResult Index(String initiale = null)
        {
            DbSqlQuery<Album> album = null;
            if (initiale != null)
                album = db.Album.SqlQuery("SELECT DISTINCT * FROM Album WHERE Album.Titre_Album LIKE '" + initiale + "%' ORDER BY Album.Titre_Album ;");
            else
                album = db.Album.SqlQuery("SELECT DISTINCT * FROM Album ORDER BY Album.Titre_Album ;");

            return View(album.ToList());
        }

        public class BinaryResult : ActionResult
        {

            public string ContentType { get; set; }
            public byte[] Data { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = ContentType;

                if (Data != null)
                    response.BinaryWrite(Data);
            }
        }

        public ActionResult Pochette(int id)
        {
            // On récupère la photo dans la base de donnée et on la retourne telle quelle.
            byte[] photo = db.Album.Where(i => i.Code_Album == id).Single().Pochette;

            return new BinaryResult() { ContentType = "image/jpeg", Data = photo };
        }

        public ActionResult Extrait(int id)
        {
            byte[] audio = null;
            // On récupère la photo dans la base de donnée et on la retourne telle quelle.
            try
            {
                audio = db.Enregistrement.Where(e => e.Code_Morceau == id).Single().Extrait;
            }
            catch (System.InvalidOperationException)
            {
                return null;
            }
            return new BinaryResult() { ContentType = "audio/mp3", Data = audio };
        }

        public ActionResult Album(int id = -1)
        {
            DbSqlQuery<Album> album = null;
            if (id != -1)
                album = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Disque ON Album.Code_Album = Disque.Code_Album JOIN Composition_Disque ON Disque.Code_Disque = Composition_Disque.Code_Disque JOIN Enregistrement ON Composition_Disque.Code_Morceau = Enregistrement.Code_Morceau JOIN Interpréter ON Enregistrement.Code_Morceau = Interpréter.Code_Morceau JOIN Instrument ON Interpréter.Code_Instrument = Instrument.Code_Instrument WHERE Instrument.Code_Instrument = '" + id + "';");
            else
                album = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album;");

            return View(album.ToList());
        }

        public ActionResult AlbumGenre(int id = -1)
        {
            DbSqlQuery<Album> album = null;
            if (id != -1)
                album = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album JOIN Genre ON Album.Code_Genre = Genre.Code_Genre WHERE Album.Code_Genre = '" + id + "';");
            else
                album = db.Album.SqlQuery("SELECT DISTINCT Album.* FROM Album;");

            return View(album.ToList());
        }


        //
        // GET: /Album/Details/5

        public ActionResult Details(int id = 0, int idMorceau = 0)
        {
            ViewData["IdMorceau"] = idMorceau;

            Album album = db.Album.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }

            DbSqlQuery<Enregistrement> enregistrement = db.Enregistrement.SqlQuery("SELECT Enregistrement.* FROM Enregistrement JOIN Composition_Disque ON Enregistrement.Code_Morceau = Composition_Disque.Code_Morceau JOIN Disque ON Composition_Disque.Code_Disque = Disque.Code_Disque JOIN Album ON Disque.Code_Album = Album.Code_Album WHERE Album.Code_Album = " + id + ";");
            ViewBag.Extrait = enregistrement.ToList();

            return View(album);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}