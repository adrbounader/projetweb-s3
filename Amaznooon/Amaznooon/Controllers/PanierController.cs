﻿using Amaznooon.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    [Authorize]
    public class PanierController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Panier/

        public ActionResult Index()
        {
            if (Session["Panier"] != null)
                return View(this.Session["Panier"] as List<Enregistrement>);
            else
                return RedirectToAction("Login", "Compte");
        }

        //
        // GET: /Panier/Details/5

        public ActionResult AddArticle(int enregistrement, int album)
        {
            AjoutAuPanier(enregistrement);
            return RedirectToAction("Details", "Album", new { id = album });
        }

        public ActionResult Delete(int idEnregistrement)
        {
            if (Session["Panier"] != null)
            {
                EnleverDuPanier(idEnregistrement);
                return RedirectToAction("Index");
            }
            else
                return RedirectToAction("Login", "Compte");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Acheter()
        {
            if(Session["Panier"] != null)
            {
                try
                {
                    validationAchat();
                    viderPanier();
                }
                catch(Exception)
                {
                    return RedirectToAction("Index", "Panier");
                }
            }
            return RedirectToAction("Index", "Panier");
        }

        public ActionResult DejaAcheter(int id = 0)
        {
            ViewData["IdMorceau"] = id; 
            Abonné user = this.Session["User"] as Abonné;

            return View(GetEnregistrementsAchetés(user.Code_Abonné));
        }

        #region Applications auxiliaires
        private void AjoutAuPanier(int Enregistrement)
        {
            Enregistrement enregistrement = db.Enregistrement.Find(Enregistrement);

            List<Enregistrement> panier = this.Session["Panier"] as List<Enregistrement>;
            panier.Add(enregistrement);
            this.Session["Panier"] = panier;
            this.Session["NombreArticles"] = panier.Count();

        }

        private List<Enregistrement> GetEnregistrementsAchetés(int idAbonné)
        {
            var enregistrement = db.Achat.Where(a => a.Code_Abonné == idAbonné).Select(a => a.Enregistrement);

            return enregistrement.ToList();
        }

        private void EnleverDuPanier(int idEnregistrement)
        {
            List<Enregistrement> panier = this.Session["Panier"] as List<Enregistrement>;

            Enregistrement Enregistrement = null;

            foreach(Enregistrement enregistrement in panier)
            {
                if (enregistrement.Code_Morceau == idEnregistrement)
                {
                    Enregistrement = enregistrement;
                    break;
                }
            }

            if (Enregistrement != null)
            {
                panier.Remove(Enregistrement);
            }

            this.Session["NombreArticles"] = panier.Count();
        }

        private void viderPanier()
        {
            List<Enregistrement> panier = this.Session["Panier"] as List<Enregistrement>;
            panier.Clear();

            this.Session["NombreArticles"] = panier.Count();
        }

        private int GetNombreAchats()
        {
            List<Enregistrement> panier = this.Session["Panier"] as List<Enregistrement>;
            return panier.Count;
        }

        private void validationAchat()
        {
            Abonné abonné = this.Session["User"] as Abonné;

            List<Enregistrement> panier = this.Session["Panier"] as List<Enregistrement>;
            //création des achats pour chaques enregistrement dans le panier
            foreach (Enregistrement enregistrement in panier)
            {
                abonné.Achat.Add(new Achat() { Code_Enregistrement = enregistrement.Code_Morceau, Code_Abonné = abonné.Code_Abonné});
            }
            //sauvegarde des changements
            db.SaveChanges();
        }
        
        #endregion
    }
}