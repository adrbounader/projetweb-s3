﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amaznooon.Controllers
{
    public class InstrumentController : Controller
    {
        private Classique_WebEntities db = new Classique_WebEntities();

        //
        // GET: /Instrument/

        public ActionResult Index(String initiale = null)
        {
            DbSqlQuery<Instrument> instrument = null;
            if (initiale != null)
                instrument = db.Instrument.SqlQuery("SELECT DISTINCT * FROM Instrument WHERE Instrument.Nom_Instrument LIKE '" + initiale + "%' ORDER BY Instrument.Nom_Instrument ;");
            else
                instrument = db.Instrument.SqlQuery("SELECT DISTINCT * FROM Instrument ORDER BY Instrument.Nom_Instrument ;");

            return View(instrument.ToList());
        }

        public class BinaryResult : ActionResult
        {
            public string ContentType { get; set; }
            public byte[] Data { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = ContentType;

                if (Data != null)
                    response.BinaryWrite(Data);
            }
        }

        public ActionResult Image(int id)
        {
            // On récupère la photo dans la base de donnée et on la retourne telle quelle.
            byte[] photo = db.Instrument.Where(i => i.Code_Instrument == id).Single().Image;

            return new BinaryResult() { ContentType = "image/jpeg", Data = photo };
        }

        //
        // GET: /Instrument/Details/5

        public ActionResult Details(int id = 0)
        {
            Instrument instrument = db.Instrument.Find(id);
            if (instrument == null)
            {
                return HttpNotFound();
            }
            return View(instrument);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
